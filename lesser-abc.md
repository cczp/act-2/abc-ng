The point of this experiment is to reduce the English alphabet to as
few letters as possible while still making it possible to spell words
without too much problem.

## Basic rules

* Everything is spelled (almost) exactly as it sounds like.
* Stressed General American pronunciation of individual words is
  assumed.

## Letters

There are no lowercase letters.

| Letter/combination | Sounds       |
| ------------------ | ------------ |
| A                  | a, ʌ, ɑ, æ   |
| B                  | b            |
| C                  | k, g         |
| CH                 | tʃ           |
| D                  | d            |
| E                  | ɛ, ɜ, e, ə\* |
| H                  | h            |
| I                  | i, ɪ, j      |
| L                  | l            |
| M                  | m            |
| N                  | n            |
| NG                 | ŋ            |
| O                  | ɔ, o         |
| P                  | p            |
| PH                 | f            |
| R                  | ɹ            |
| S                  | s            |
| SH                 | ʃ            |
| T                  | t            |
| TH                 | θ, ð         |
| V                  | u, ʊ, v, w   |
| Z                  | z            |
| ZH                 | ʒ            |

\*Nothing is written if the /ə/ is between 2 consonants.

## Letter names

Are kept from the original English but are spelled according to the
new rules. For example, "aitch" (for the letter "h") is spelled "EICH."

## Example text

AR THOVS SHAI IVREIZHN PHVTWER, CAVBOI CHAPS, OR DZHOLI ERTHMOVINC
HEDCIER?

Are those shy Eurasian footwear, cowboy chaps, or jolly earthmoving
headgear?
